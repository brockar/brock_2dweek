﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameObject player;
    public Text txt;

    // Start is called before the first frame update
    void Start()
    {
        txt.text = "Score: 0";
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "Score: " + player.GetComponent<PlayerMovement>().GetScore();
    }
}
