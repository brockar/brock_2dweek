﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb2D;
    public Animator pAnimator;
    public SpriteRenderer spriteRenderer;

    public float runSpeed;
    public float jumpForce;
    public int score;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
        
    }

    private void FixedUpdate()
    {
        float hInput = Input.GetAxis("Horizontal");

        //sets velocity for y axis as... static
        rb2D.velocity = new Vector2(hInput * runSpeed * Time.deltaTime, rb2D.velocity.y);

        if (rb2D.velocity.x > 0f)
        {
            spriteRenderer.flipX = false;
        }
        else if (rb2D.velocity.x < 0f)
        {
            spriteRenderer.flipX = true;
        }

        if (Mathf.Abs(hInput) > 0f)
        {
            pAnimator.SetBool("IsRunning", true);
        }
        else
        {
            pAnimator.SetBool("IsRunning", false);
        }
    }

    void Jump()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
    }

    public void GetGem(int value)
    {
        score += value;
    }

    public int GetScore()
    {
        return score;
    }
}
